/**
 * *****************************************************************************
 * Copyright (C) 2020 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.rest;

import com.mongodb.BasicDBObject;
import com.mongodb.ErrorCategory;
import com.mongodb.MongoClient;
import com.mongodb.MongoWriteException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.model.ReturnDocument;
import com.mongodb.client.result.DeleteResult;
import es.bsc.inb.elixir.openebench.rest.validator.JsonMessageValidator;
import es.bsc.inb.elixir.openebench.rest.validator.PKValidationError;
import es.bsc.inb.elixir.openebench.rest.validator.ValidationException;
import es.elixir.bsc.json.schema.ValidationError;
import java.io.Writer;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.spi.Unmanaged;
import javax.inject.Inject;
import javax.inject.Named;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import javax.servlet.ServletContext;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.bson.AbstractBsonWriter;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.DocumentCodec;
import org.bson.codecs.EncoderContext;
import org.bson.json.JsonWriter;
import org.bson.json.JsonWriterSettings;

/**
 * @author Dmitry Repchevsky
 */

@Named
@ApplicationScoped
public class MongoDBBenchmarksBean {
    
    private final static String PROVENANCE_PROPERTY = "@provenance";
    
    private String database;
    private String temp_db;

    private String schemas_location_uri;
    
    private Map<String, JsonMessageValidator> validators;
    
    @Inject private MongoClient mc;
    @Inject private ServletContext context;
    
    @PostConstruct
    protected void init() {
        database = context.getInitParameter("benchmarking.db");
        temp_db = context.getInitParameter("benchmarking.temp.db");
        
        schemas_location_uri = context.getInitParameter("json.schemas.uri");
                
        validators = new ConcurrentHashMap<>();
    }

   public String getJsonSchemaURI(final String collection) {
        final StringBuilder uri = new StringBuilder();
        uri.append(schemas_location_uri);
        uri.append(collection);
        uri.append(".json");
        
        uri.setCharAt(schemas_location_uri.length(), Character.toLowerCase(collection.charAt(0)));

        return uri.toString();
   }
   
   public JsonObject getJsonSchema(final String collection) {
       final JsonMessageValidator validator = getValidator(getJsonSchemaURI(collection));
       return validator.getJsonSchema();
   }

   public void write(Writer writer, String collection) {
        try {
            MongoDatabase db = mc.getDatabase(temp_db);
            MongoCollection<Document> col = db.getCollection(collection);            
            try (JsonWriter jwriter = new ReusableJsonWriter(writer)) {

                final DocumentCodec codec = new DocumentCodec() {
                    @Override
                    public void encode(BsonWriter writer,
                       Document document,
                       EncoderContext encoderContext) {
                            super.encode(jwriter, document, encoderContext);
                    }
                };
                jwriter.writeStartArray();
                
                FindIterable<Document> iterator = col.find();

                try (MongoCursor<Document> cursor = iterator.iterator()) {
                    while (cursor.hasNext()) {
                        final Document doc = cursor.next();
                        doc.remove(PROVENANCE_PROPERTY);
                        doc.toJson(codec);
                    }
                }
                jwriter.writeEndArray();
            }
        } catch(Exception ex) {
            Logger.getLogger(MongoDBBenchmarksBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Writes all identifies found in the temporary database for the provided collection
     * 
     * @param writer writer to write the identifiers.
     * @param collection collection where to get the identifiers from.
     * @param username owner of identifiers
     */
    public void writeTempIds(final Writer writer,
                             final String collection,
                             final String username) {
        writeIds(writer, temp_db, collection, username);
    }

    public void writePublicIds(final Writer writer,
                               final String collection,
                               final String username) {
        writeIds(writer, database, collection, username);
    }
    
    private void writeIds(final Writer writer, 
                         final String database,
                         final String collection,
                         final String username) {
        try {
            MongoDatabase db = mc.getDatabase(database);
            MongoCollection<Document> col = db.getCollection(collection);            
                
            FindIterable<Document> iterator = col.find().projection(new BasicDBObject("_id", true));

            try (MongoCursor<Document> cursor = iterator.iterator()) {
                while (cursor.hasNext()) {
                    final Document doc = cursor.next();
                    writer.write(doc.getString("_id"));
                    writer.write('\n');
                }
            }
        } catch(Exception ex) {
            Logger.getLogger(MongoDBBenchmarksBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Boolean hasObject(final String id, 
                             final String collection,
                             final String username) {
        try {
            MongoDatabase db = mc.getDatabase(temp_db);
            MongoCollection<Document> col = db.getCollection(collection);
            
            return col.countDocuments(Filters.eq("_id", id)) > 0;
//            return col.countDocuments(Filters.and(Filters.eq("_id", id), 
//                    Filters.eq(PROVENANCE_PROPERTY, username))) > 0;
        } catch(Exception ex) {
            Logger.getLogger(MongoDBBenchmarksBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Boolean removeObject(final String id, 
                                final String collection,
                                final String username) {
        
        try {
            MongoDatabase db = mc.getDatabase(temp_db);
            MongoCollection<Document> col = db.getCollection(collection);

            final DeleteResult result = col.deleteOne(Filters.and(Filters.eq("_id", id), 
                    Filters.eq(PROVENANCE_PROPERTY, username)));
            
            return result.getDeletedCount() > 0;
        } catch(Exception ex) {
            Logger.getLogger(MongoDBBenchmarksBean.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        return null;
    }
    
    /**
     * 
     * @param id object identifier.
     * @param collection mongodb collection search is performed.
     * @param username 
     * 
     * @return either found object (json) or null.
     */
    public String getObject(final String id, 
                            final String collection,
                            final String username) {
        
        try {
            MongoDatabase db = mc.getDatabase(temp_db);
            MongoCollection<Document> col = db.getCollection(collection);

            Document doc = col.find(Filters.eq("_id", id)).first();
//            Document doc = col.find(Filters.and(Filters.eq("_id", id), 
//                    Filters.eq(PROVENANCE_PROPERTY, username))).first();
            if (doc == null) {
                return null;
            }

            doc.remove(PROVENANCE_PROPERTY);
            return doc.toJson();
        } catch(Exception ex) {
            Logger.getLogger(MongoDBBenchmarksBean.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        return null;
    }
    
    /**
     * Inserts json object into the mongodb
     * 
     * @param object object to be inserted
     * @param username provenance (email)
     * 
     * @return the ID of inserted object.
     * 
     * @throws ValidationException
     */
    public String putObject(final JsonObject object,
                            final String username) throws ValidationException {
        
        final JsonValue value = object.get("_schema");
        if (value == null) {
            throw new WebApplicationException("no object '_schema' specified", Response.Status.BAD_REQUEST);
        }
        if (value.getValueType() != ValueType.STRING) {
            throw new WebApplicationException("object '_schema' must be a string", Response.Status.BAD_REQUEST);
        }
        
        final URI _schema;
        try {
            _schema = URI.create(((JsonString)value).getString());
        } catch(IllegalArgumentException ex) {
            throw new WebApplicationException("'_schema' must be a valid URI", Response.Status.BAD_REQUEST);
        }
        
        final String[] path = _schema.getPath().split("/");
        final String collection = path[path.length - 1];

        return putObject(collection, object, username);
    }
    
    /**
     * Inserts json object into the mongodb collection
     * 
     * @param collection collection object belongs to.
     * @param object object to be inserted.
     * @param username object provenance (email)
     * 
     * @return the ID of inserted object.
     * 
     * @throws ValidationException
     */
    public String putObject(final String collection, 
                            final JsonObject object,
                            final String username) throws ValidationException {
        final JsonValue _id = object.get("_id");
        if (_id == null) {
            throw new RuntimeException("no object '_id' found");
        }
        if (_id.getValueType() != ValueType.STRING) {
            throw new RuntimeException("object '_id' must be a string");
        }

        final String id = ((JsonString)_id).getString();
        
        final String json = object.toString();
        putObject(collection, id, json, username);
        
        return id;
    }
    
    public void putObject(final String collection,
                          final String id, 
                          final String json,
                          final String username) throws ValidationException {
        insertObject(collection, id, json, username, false);
    }

    public String patchObject(final String collection,
                              final String id, 
                              final String json,
                              final String username) throws ValidationException {
        return insertObject(collection, id, json, username, true);
    }
    
    private String insertObject(final String collection, 
                                final String id, 
                                final String json,
                                final String username,
                                final boolean upsert) throws ValidationException {

        final String communicty_code = getCommunityCode(username);
        if (communicty_code == null) {
            throw new ValidationException("/", 
                    Arrays.asList(new ValidationError(String.format("no cummunity code for the user %s found", username))));
        }
        final JsonMessageValidator validator = getValidator(getJsonSchemaURI(collection));

        final List<ValidationError> errors = new ArrayList<>();
        final String json2 = validator.isValid(errors, communicty_code, json, false, true); // !upsert -> false

        if (!errors.isEmpty()) {
            throw new ValidationException(id, errors);
        }
        
        final String _id = OpenEBenchKeysValidationBean.getId(communicty_code, collection, id);
        if (_id == null) {
            Logger.getLogger(MongoDBBenchmarksBean.class.getName()).log(Level.INFO, "invalid id {0}", id);
            throw new ValidationException("/", 
                    Arrays.asList(new ValidationError(String.format("invalid OEB identifier: %s", id))));
        }
        
        try {
            final MongoDatabase db = mc.getDatabase(temp_db);
            MongoCollection<Document> col = db.getCollection(collection);

            Document doc = Document.parse(json2);
            doc.put("_id", _id);
            doc.append(PROVENANCE_PROPERTY, username);
            
            if (!_id.equals(id)) {
                doc.append("orig_id", id);
            }

            if (upsert) {
                FindOneAndUpdateOptions opt = new FindOneAndUpdateOptions().upsert(true)
                                .returnDocument(ReturnDocument.AFTER);
                Document result = col.findOneAndUpdate(Filters.and(
                                      Filters.eq("_id", _id), Filters.eq(PROVENANCE_PROPERTY, username)),
                                      new Document("$set", doc), opt);
                if (result != null) {
                    return result.toJson();
                }
            } else {
                col.insertOne(doc);
            }
        } catch(MongoWriteException ex) {
            if (ErrorCategory.DUPLICATE_KEY == ErrorCategory.fromErrorCode(ex.getCode() )) {
                throw new ValidationException("/", 
                        Arrays.asList(new ValidationError(String.format(PKValidationError.MESSAGE, id))));
            }
            Logger.getLogger(MongoDBBenchmarksBean.class.getName()).log(Level.SEVERE, null, ex);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        } catch(Exception ex) {
            Logger.getLogger(MongoDBBenchmarksBean.class.getName()).log(Level.SEVERE, null, ex);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }        
        
        return null;
    }
    
    private JsonMessageValidator getValidator(final String schemaLocation) {
        JsonMessageValidator validator = validators.get(schemaLocation);
        if (validator == null) {
            try {
                Unmanaged<JsonMessageValidator> unmanaged = new Unmanaged<>(JsonMessageValidator.class); 
                Unmanaged.UnmanagedInstance<JsonMessageValidator> umc = unmanaged.newInstance(); 
                umc.produce().inject().postConstruct(); 
                validator = umc.get();

                validator.initialize(schemaLocation);

                validators.put(schemaLocation, validator);
            } catch(Exception ex) {
                Logger.getLogger(MongoDBBenchmarksBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return validator;
    }
    
    private String getCommunityCode(final String username) {

        try {
            final MongoDatabase db = mc.getDatabase(database);
            
            final MongoCollection<Document> contacts = db.getCollection("Contact");
            
            final Document contact = contacts.find(new BasicDBObject("email", username)).first();
            if (contact != null) {
                String community_id = contact.get("community_id", String.class);
                if (community_id == null || community_id.length() != 7) {
                    final String contact_id = contact.get("_id", String.class);
                    final MongoCollection<Document> communities = db.getCollection("Community");
                    final Document community = communities.find(Filters.in("community_contact_ids", contact_id)).first();
                    if (community != null) {
                        community_id = community.get("_id", String.class);
                    }
                }
                if (community_id != null && community_id.length() == 7) {
                    return community_id.substring(4);
                }
            }
            Logger.getLogger(MongoDBBenchmarksBean.class.getName()).log(Level.INFO, "no community found for user {0}", username);
        } catch(Exception ex) {
            Logger.getLogger(MongoDBBenchmarksBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static class ReusableJsonWriter extends JsonWriter {
        
        public ReusableJsonWriter(Writer writer) {
            super(writer, JsonWriterSettings.builder().indent(true).build());
        }

        @Override
        protected boolean checkState(final AbstractBsonWriter.State[] validStates) {
            return true;
        }
    }
}
