/**
 * *****************************************************************************
 * Copyright (C) 2017 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.rest.validator;

import es.bsc.inb.elixir.openebench.rest.OpenEBenchKeysValidationBean;
import es.elixir.bsc.json.schema.JsonSchemaException;
import es.elixir.bsc.json.schema.JsonSchemaReader;
import es.elixir.bsc.json.schema.ValidationError;
import es.elixir.bsc.json.schema.ext.ExtendedJsonSchemaLocator;
import es.elixir.bsc.json.schema.model.JsonObjectSchema;
import es.elixir.bsc.json.schema.model.JsonStringSchema;
import es.elixir.bsc.json.schema.model.PrimitiveSchema;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonString;
import javax.json.JsonStructure;
import javax.json.JsonValue;
import javax.servlet.ServletContext;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * JSON message validator
 * 
 * @author Dmitry Repchevsky
 */

public class JsonMessageValidator implements ConstraintValidator<JsonSchema, String> {

    @Inject
    private ServletContext ctx;
    
    @Inject
    private OpenEBenchKeysValidationBean fkvalidator;

    private ExtendedJsonSchemaLocator locator;
    private es.elixir.bsc.json.schema.model.JsonSchema schema;
    
    public JsonMessageValidator() {}
    
    @Override
    public void initialize(final JsonSchema annotation) {
        initialize(annotation.location());
    }

    public void initialize(final String location) {
        try {
            final URL url = ctx.getResource("/").toURI().resolve(location).toURL();
            if (url != null) {
                locator = new GitHubSchemaLocator(url.toURI());
                schema = JsonSchemaReader.getReader().read(locator);
            }
        } catch (IOException | JsonSchemaException | URISyntaxException ex) {
            Logger.getLogger(JsonMessageValidator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public JsonObject getJsonSchema() {
        return locator.getSchemas(schema.getId()).get("/");
    }
    
    @Override
    public boolean isValid(String json, ConstraintValidatorContext context) {
        
        final List<ValidationError> errors = new ArrayList<>();
        isValid(errors, null, json, false, false);
        return errors.isEmpty();
    }

    public String isValid(final List<ValidationError> errors,
                          final String community, 
                          final String json, 
                          final boolean pk, 
                          final boolean fk) {

        if (schema == null) {
            errors.add(new ValidationError("no json schema found"));
            return json;
        }
        
        final JsonStructure data = Json.createReader(new StringReader(json)).read();
        
        final Map<JsonValue, JsonValue> replace_map = new IdentityHashMap<>();
        
        schema.validate(data, errors, (PrimitiveSchema model, JsonValue value, List<ValidationError> err) -> {
            
            if (pk && model instanceof JsonObjectSchema && 
                "/".equals(model.getJsonPointer())) { // look only in ROOT schema(s)
                
                JsonObject object = locator.getSchemas(model.getId()).get(model.getJsonPointer());
                
                JsonValue pkValue = object.get("primary_key");
                if (pkValue != null) {
                    if (JsonValue.ValueType.ARRAY == pkValue.getValueType()) {
                        final Map<String, String> pkMap = new HashMap<>();
                        final JsonArray pkeys = pkValue.asJsonArray();
                        for (int i = 0, n = pkeys.size(); i < n; i++) {
                            JsonValue pkey = pkeys.get(i);
                            if (JsonValue.ValueType.STRING == pkey.getValueType()) {
                                final String property = ((JsonString)pkey).getString();
                                final JsonValue keyValue = value.asJsonObject().get(property);
                                if (JsonValue.ValueType.STRING == keyValue.getValueType()) {
                                    pkMap.put(property, ((JsonString)keyValue).getString());
                                }
                            }
                        }
                        if (!fkvalidator.validatePKs(pkMap, value.asJsonObject(), community)) {
                            err.add(new FKValidationError(model.getId(), model.getJsonPointer(), pkMap.toString()));
                        }
                    }
                }
            } else if (fk && model instanceof JsonStringSchema &&
                JsonValue.ValueType.STRING == value.getValueType()) {
                JsonObject object = locator.getSchemas(model.getId()).get(model.getJsonPointer());
                JsonValue fkValue = object.get("foreign_keys");
                if (fkValue != null && JsonValue.ValueType.ARRAY == fkValue.getValueType()) {
                    final String fk_value = ((JsonString)value).getString();
                    final StringBuilder fk_wrapper = new StringBuilder(fk_value);
                    if (fkvalidator.validateFKs(fk_wrapper, fkValue.asJsonArray(), community)) {
                        final String new_fk_value = fk_wrapper.toString();
                        if (!fk_value.equals(new_fk_value)) {
                            replace_map.put(value, Json.createValue(new_fk_value));
                        }

                    } else {
                        err.add(new FKValidationError(model.getId(), model.getJsonPointer(), ((JsonString)value).getString()));
                    }
                }
            }
        });

        if (replace_map.isEmpty()) {
            return json; // no FK replacements
        }
        
        return replace(data, replace_map);
    }
    
    private String replace(final JsonStructure data, 
                           final Map<JsonValue, JsonValue> replace_map) {
        if (data instanceof JsonObject) {
            final JsonObjectBuilder builder = Json.createObjectBuilder();
            replace(builder, data.asJsonObject(), replace_map);
            return builder.build().toString();
        }
        
        if (data instanceof JsonArray) {
            final JsonArrayBuilder builder = Json.createArrayBuilder();
            replace(builder, data.asJsonArray(), replace_map);
            return builder.build().toString();
        }
        
        return null;
    }
    
    private void replace(final JsonObjectBuilder builder,
                         final JsonObject data,
                         final Map<JsonValue, JsonValue> replace_map) {
        for (Map.Entry<String, JsonValue> entry : data.entrySet()) {
            final JsonValue value = entry.getValue();
            switch(value.getValueType()) {
                case OBJECT: final JsonObjectBuilder ob = Json.createObjectBuilder();
                             replace(ob, value.asJsonObject(), replace_map);
                             builder.add(entry.getKey(), ob);
                             break;
                case ARRAY:  final JsonArrayBuilder ab = Json.createArrayBuilder();
                             replace(ab, value.asJsonArray(), replace_map);
                             builder.add(entry.getKey(), ab);
                             break;
                case STRING: final JsonValue new_value = replace_map.getOrDefault(value, value);
                             builder.add(entry.getKey(), new_value);
                             break;
                default:     builder.add(entry.getKey(), value);
            }
        }
        
    }

    private void replace(final JsonArrayBuilder builder,
                         final JsonArray data,
                         final Map<JsonValue, JsonValue> replace_map) {
        
        for (JsonValue value : data) {
            switch(value.getValueType()) {
                case OBJECT: final JsonObjectBuilder ob = Json.createObjectBuilder();
                             replace(ob, value.asJsonObject(), replace_map);
                             builder.add(ob);
                             break;
                case ARRAY:  final JsonArrayBuilder ab = Json.createArrayBuilder();
                             replace(ab, value.asJsonArray(), replace_map);
                             builder.add(ab);
                             break;
                case STRING: final JsonValue new_value = replace_map.getOrDefault(value, value);
                             builder.add(new_value);
                             break;
                default:     builder.add(value);
            }
        }
    }
}
