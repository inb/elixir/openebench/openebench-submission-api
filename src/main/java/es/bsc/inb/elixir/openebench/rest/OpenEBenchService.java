/**
 * *****************************************************************************
 * Copyright (C) 2017 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.rest;

import es.bsc.inb.elixir.openebench.rest.validator.FKValidationError;
import es.bsc.inb.elixir.openebench.rest.validator.ValidationException;
import es.elixir.bsc.json.schema.ValidationError;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.net.URLDecoder;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.stream.JsonGenerator;
import javax.json.stream.JsonParser;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Encoded;
import javax.ws.rs.GET;
import javax.ws.rs.HEAD;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.StreamingOutput;

/**
 * @author Dmitry Repchevsky
 */

@RequestScoped
@Path("/")
public class OpenEBenchService {

    @Target({ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @HttpMethod("PATCH")
    public @interface PATCH {}
    
    @Inject
    private MongoDBBenchmarksBean bean;

    @Path("/{path: .*}")
    @OPTIONS
    public Response compliance() {
        return Response.ok().header("Allow", "GET")
                            .header("Allow", "HEAD")
                            .header("Allow", "PUT")
                            .header("Allow", "POST")
                            .header("Allow", "PATCH")
                            .header("Allow", "DELETE")
                            .build();
    }

    @GET
    @Path("/")
    @Hidden
    public Response get(@Context ServletContext ctx) {
        return Response.ok(ctx.getResourceAsStream("/index.html"), MediaType.TEXT_HTML).build();
    }

    @GET
    @Operation(summary = "Lists the collection data",
        responses = {
            @ApiResponse(content = @Content(
               mediaType = "text/uri-list"
//               array = @ArraySchema(schema = @Schema(
//                   ref="https://openebench.bsc.es/monitor/tool/tool.json")
            ))
        }
    )
    @Path("/{collection}")
    @Produces("text/uri-list")
    public Response getIds(@Context SecurityContext sc,
                           @PathParam("collection") 
                           @Parameter(description = "data collection",
                                      example = "BenchmarkingEvent")
                           final String collection) {

        final Principal principal = sc.getUserPrincipal();
        final String username = principal == null ? null : principal.getName();

        StreamingOutput stream = (OutputStream out) -> {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"))) {
                bean.writeTempIds(writer, collection, username);
            } catch(Exception ex) {
                Logger.getLogger(OpenEBenchService.class.getName()).log(Level.SEVERE, null, ex);
            }
        };
        return Response.ok(stream).build();
    }

    @GET
    @Operation(summary = "Get a Json Schema location for the collection",
        responses = {
            @ApiResponse(content = @Content(
               mediaType = "text/x-uri"
            ))
        }
    )
    @Path("/{collection}")
    @Produces("text/x-uri")
    public Response getJsonSchemaURI(@PathParam("collection") 
                                     @Parameter(description = "data collection",
                                                example = "BenchmarkingEvent")
                                     final String collection) {

        final String uri = bean.getJsonSchemaURI(collection);

        return Response.ok(uri).build();
    }

    @GET
    @Operation(summary = "Get a Json Schema for the collection",
        responses = {
            @ApiResponse(content = @Content(
               mediaType = "application/schema+json"
            ))
        }
    )
    @Path("/{collection}")
    @Produces("application/schema+json")
    public Response getJsonSchema(@PathParam("collection") 
                                  @Parameter(description = "data collection",
                                             example = "BenchmarkingEvent")
                                  final String collection) {

        final JsonObject schema = bean.getJsonSchema(collection);

        return Response.ok(schema).build();
    }
    
    @GET
    @Operation(summary = "Lists the collection data",
        responses = {
            @ApiResponse(content = @Content(
               mediaType = MediaType.APPLICATION_JSON
//               array = @ArraySchema(schema = @Schema(
//                   ref="https://openebench.bsc.es/monitor/tool/tool.json")
            ))
        }
    )
    @Path("/{collection}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getObjects(@PathParam("collection") 
                               @Parameter(description = "data collection",
                                          example = "BenchmarkingEvent")
                               final String collection) {
        StreamingOutput stream = (OutputStream out) -> {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"))) {
                bean.write(writer, collection);
            } catch(Exception ex) {
                Logger.getLogger(OpenEBenchService.class.getName()).log(Level.SEVERE, null, ex);
            }
        };
        return Response.ok(stream, MediaType.APPLICATION_JSON).build();
    }

    @GET
    @Operation(summary = "Get object",
        responses = {
            @ApiResponse(content = @Content(
               mediaType = MediaType.APPLICATION_JSON
//               array = @ArraySchema(schema = @Schema(
//                   ref="https://openebench.bsc.es/monitor/tool/tool.json")
            )),
            @ApiResponse(responseCode = "400", description = "invalid id"),
            @ApiResponse(responseCode = "404", description = "object not found")
        }
    )
    @Path("/{collection}/{id : .*}")
    public Response getObject(@Context SecurityContext sc,
                              @PathParam("collection")
                              @Parameter(description = "data collection",
                                         example = "BenchmarkingEvent")
                              final String collection,
                              @PathParam("id")
                              @Parameter(description = "object id",
                                         example = "CAMEO-3D:27")
                              @Encoded final String id) {

        final Principal principal = sc.getUserPrincipal();
        final String username = principal == null ? null : principal.getName();
        
        try {
            final String _id = URLDecoder.decode(id, "UTF-8");
            final String message = bean.getObject(_id, collection, username);
            if (message == null || message.isEmpty()) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            return Response.ok(message, MediaType.APPLICATION_JSON).build();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(OpenEBenchService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
    
    @HEAD
    @Operation(
        summary = "Check whether the object exists",
        responses = {
            @ApiResponse(responseCode = "200", description = "object exists"),
            @ApiResponse(responseCode = "400", description = "invalid id"),
            @ApiResponse(responseCode = "404", description = "object not found")
        }
    )
    @Path("/{collection}/{id : .*}")
    public Response hasObject(@Context final SecurityContext sc,
                              @PathParam("collection")
                              @Parameter(description = "data collection",
                                         example = "BenchmarkingEvent")
                              final String collection,
                              @PathParam("id")
                              @Parameter(description = "object id",
                                         example = "CAMEO-3D:27")
                              @Encoded final String id) {
        
        final Principal principal = sc.getUserPrincipal();
        final String username = principal == null ? null : principal.getName();
        
        try {
            final String _id = URLDecoder.decode(id, "UTF-8");
            final Boolean exist = bean.hasObject(_id, collection, username);
            return exist != null && exist ? 
                    Response.ok(MediaType.APPLICATION_JSON).build() : 
                    Response.status(Response.Status.NOT_FOUND).build();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(OpenEBenchService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @PUT
    @Operation(
        summary = "Put the object into the database",
        responses = {
            @ApiResponse(responseCode = "201", description = "object has been put"),
            @ApiResponse(responseCode = "400", description = "validation error")
        }
    )
    @Path("/{collection}/{id : .*}")
    @RolesAllowed("oeb-data-submitter")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addObject(@Context final SecurityContext sc,
                              @PathParam("collection")
                              @Parameter(description = "data collection",
                                         example = "BenchmarkingEvent")
                              final String collection, 
                              @PathParam("id") 
                              @Parameter(description = "object id",
                                         example = "CAMEO-3D:27")
                              final String id,
                              @Parameter(description = "json object")
                              final String message) {

        final Principal principal = sc.getUserPrincipal();
        final String username = principal == null ? null : principal.getName();
        
        try {
            final String _id = URLDecoder.decode(id, "UTF-8");
            bean.putObject(collection, _id, message, username);  
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(OpenEBenchService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        } catch (ValidationException ex) {
            final JsonArrayBuilder jb = Json.createArrayBuilder();
            for (ValidationError error : ex.errors) {
                if (error.id == null) {
                    jb.add(Json.createObjectBuilder()
                        .add("error", error.message));
                } else {
                    final String[] path = error.id.getPath().split("/");
                    jb.add(Json.createObjectBuilder()
                            .add("schema", path[path.length - 1])
                            .add("pointer", error.pointer)
                            .add("error", error.message));
                }
            }
            return Response.status(Response.Status.BAD_REQUEST).entity(jb.build()).build();
        }
        
        return Response.status(Response.Status.CREATED).build();
    }
        
    @PATCH
    @Operation(
        summary = "Patch the object using mongodb 'upsert' operation",
        responses = {
            @ApiResponse(responseCode = "201", description = "object has been put"),
            @ApiResponse(responseCode = "400", description = "validation error")
        }
    )
    @Path("/{collection}/{id : .*}")
    @RolesAllowed("oeb-data-submitter")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response patchObject(@Context final SecurityContext sc,
                                @PathParam("collection") 
                                @Parameter(description = "data collection",
                                           example = "BenchmarkingEvent")
                                final String collection, 
                                @PathParam("id")
                                @Parameter(description = "object id",
                                           example = "CAMEO-3D:27")
                                final String id,
                                @Parameter(description = "json object")
                                final String message) {

        final Principal principal = sc.getUserPrincipal();
        final String username = principal == null ? null : principal.getName();
        
        try {
            final String _id = URLDecoder.decode(id, "UTF-8");
            final String result = bean.patchObject(collection, _id, message, username);
            if (result == null) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            }
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(OpenEBenchService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage()).build();
        } catch (ValidationException ex) {
            Logger.getLogger(OpenEBenchService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @POST
    @Operation(
        summary = "Put multiple objects in a bulk mode",
        description = "client sends an array of objects of any type. " +
                      "the type and id are taken from the '_id' and '_schema' attributes. " +
                      "the service respects foreign keys, but sending disordered object " + 
                      "slows down processing speed and memory usage.",
        responses = {
            @ApiResponse(responseCode = "200", description = "all objects inserted"),
            @ApiResponse(responseCode = "400", description = "validation error")
        }
    )
    @Path("/")
    @RolesAllowed("oeb-data-submitter")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addObjects(@Context 
                               final SecurityContext sc, 
                               final Reader reader) {

        final Principal principal = sc.getUserPrincipal();
        final String username = principal == null ? null : principal.getName();
        
        final LinkedList<JsonObject> sinker = new LinkedList<>();
        final ArrayList<ValidationException> errors = new ArrayList<>();
        
        try (JsonParser parser = Json.createParser(reader)) {
            if (parser.hasNext() &&
                parser.next() == JsonParser.Event.START_ARRAY) {
                final Stream<JsonValue> stream = parser.getArrayStream();
                final Iterator<JsonValue> iterator = stream.iterator();
                while (iterator.hasNext()) {
                    final JsonValue item = iterator.next();
                    if (JsonValue.ValueType.OBJECT == item.getValueType()) {
                        final JsonObject object = item.asJsonObject();
                        try {
                            bean.putObject(object, username);
                        } catch(ValidationException ex) {
                            boolean fk_only = true;
                            for (ValidationError error : ex.errors) {
                                if (!(error instanceof FKValidationError)) {
                                    fk_only = false;
                                }
                            }
                            if (fk_only) {
                                sinker.add(object);
                            } else {
                                errors.add(ex);
                            }   
                        }
                    }
                }
            } else {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        }

        if (!sinker.isEmpty()) {
            final ArrayList<ValidationException> fk_errors = new ArrayList<>();

            int sz;
            do {
                sz = sinker.size();
                fk_errors.clear();
                
                for (int i = 0, n = sinker.size(); i < n; i++) {
                    final JsonObject object = sinker.poll();
                    try {
                        bean.putObject(object, username);
                    } catch(ValidationException ex) {
                        sinker.add(object);
                        fk_errors.add(ex);
                    }
                }
            } while (sinker.size() > 0 && sinker.size() < sz);
            
            errors.addAll(fk_errors);
        }
        
        if (errors.isEmpty()) {
            return Response.ok().build();
        } else {
            // return BAD_REQUEST with validation errors found.
            StreamingOutput response = (OutputStream out) -> {
                try (JsonGenerator jgenerator = Json.createGenerator(out)) {
                    jgenerator.writeStartArray();
                    for (ValidationException ex : errors) {
                        writeErrors(ex, jgenerator);
                        jgenerator.writeEnd();
                    }
                }
            };
            return Response.status(Response.Status.BAD_REQUEST).entity(response).build();
        }
    }

    @POST
    @Operation(
        summary = "Put multiple objects of the same collection in a bulk mode",
        description = "client sends an array of objects of the same type. " +
                      "the id is taken from the '_id' attribute. ",
        responses = {
            @ApiResponse(responseCode = "200", description = "all objects inserted"),
            @ApiResponse(responseCode = "400", description = "validation error")
        }
    )
    @Path("/{collection}")
    @RolesAllowed("oeb-data-submitter")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addObjects(@Context SecurityContext sc,
                               @PathParam("collection") 
                               final String collection, 
                               final Reader reader) {
        
        final Principal principal = sc.getUserPrincipal();
        final String username = principal == null ? null : principal.getName();
        
        try (JsonParser parser = Json.createParser(reader)) {
            if (parser.hasNext() &&
                parser.next() == JsonParser.Event.START_ARRAY) {
                final Stream<JsonValue> stream = parser.getArrayStream();
                final Iterator<JsonValue> iterator = stream.iterator();
                while(iterator.hasNext()) {
                    final JsonValue item = iterator.next();
                    if (JsonValue.ValueType.OBJECT == item.getValueType()) {
                        final JsonObject object = item.asJsonObject();
                        try {
                            bean.putObject(collection, object, username);
                        } catch(ValidationException ex) {
                            // once there is at least one validation error found
                            // return BAD_REQUEST and start streaming.
                            final ByteArrayOutputStream out = new ByteArrayOutputStream();
                            try (JsonGenerator jgenerator = Json.createGenerator(out)) {
                                jgenerator.writeStartArray();
                                writeErrors(ex, jgenerator);
                                while(iterator.hasNext()) {
                                    JsonValue value = iterator.next();
                                    if (JsonValue.ValueType.OBJECT == value.getValueType()) {
                                        JsonObject obj = value.asJsonObject();
                                        try {
                                            bean.putObject(collection, obj, username);
                                        } catch(ValidationException ex2) {
                                            writeErrors(ex2, jgenerator);
                                        }
                                    }
                                }
                                jgenerator.writeEnd();
                            }
                            return Response.status(Response.Status.BAD_REQUEST).entity(out.toByteArray()).build();
                        }
                    }
                }
            } else {
                parser.close();
                return Response.status(Response.Status.BAD_REQUEST).build();
            }                       
        } catch (Exception ex) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        return Response.ok().build();
    }
    
    private void writeErrors(ValidationException exception, JsonGenerator jgenerator) {
        for (ValidationError error : exception.errors) {
            jgenerator.writeStartObject();
            jgenerator.write("_id", exception.id);
            if (error.id != null) {
                final String[] path = error.id.getPath().split("/");
                jgenerator.write("_schema", path[path.length - 1]);
                jgenerator.write("pointer", error.pointer);
            }
            jgenerator.write("error", error.message);
            jgenerator.writeEnd();
        }
    }
    
    @DELETE
    @Path("/{collection}/{id : .*}")
    @RolesAllowed("oeb-data-submitter")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response deleteObject(@Context SecurityContext sc,
                                 @PathParam("collection") String collection, 
                                 @PathParam("id") String id, 
                                 @Suspended final AsyncResponse asyncResponse) {
        
        final Principal principal = sc.getUserPrincipal();
        final String username = principal == null ? null : principal.getName();
        
        try {
            final String _id = URLDecoder.decode(id, "UTF-8");

            final Boolean result = bean.removeObject(_id, collection, username);
            if (result == null) {
                return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build(); 
            }
            return result ? Response.ok().build() : Response.status(Response.Status.NOT_FOUND).build();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(OpenEBenchService.class.getName()).log(Level.SEVERE, null, ex);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    
    @GET
    @Operation(summary = "Lists the collection data",
        responses = {
            @ApiResponse(content = @Content(
               mediaType = "text/uri-list"
//               array = @ArraySchema(schema = @Schema(
//                   ref="https://openebench.bsc.es/monitor/tool/tool.json")
            ))
        }
    )
    @Path("/public/{collection}")
    @Produces("text/uri-list")
    public Response getPublicIds(@Context SecurityContext sc,
                                 @PathParam("collection") 
                                 @Parameter(description = "data collection",
                                            example = "BenchmarkingEvent")
                                 final String collection) {

        final Principal principal = sc.getUserPrincipal();
        final String username = principal == null ? null : principal.getName();

        StreamingOutput stream = (OutputStream out) -> {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(out, "UTF-8"))) {
                bean.writePublicIds(writer, collection, username);
            } catch(Exception ex) {
                Logger.getLogger(OpenEBenchService.class.getName()).log(Level.SEVERE, null, ex);
            }
        };
        return Response.ok(stream).build();
    }
}
