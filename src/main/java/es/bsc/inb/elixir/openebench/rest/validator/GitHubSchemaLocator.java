/**
 * *****************************************************************************
 * Copyright (C) 2019 Spanish National Bioinformatics Institute (INB) and
 * Barcelona Supercomputing Center
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.rest.validator;

import es.elixir.bsc.json.schema.JsonSchemaLocator;
import es.elixir.bsc.json.schema.ext.ExtendedJsonSchemaLocator;
import java.net.URI;
import java.util.Map;
import javax.json.JsonObject;

/**
 * @author Dmitry Repchevsky
 */

public class GitHubSchemaLocator extends ExtendedJsonSchemaLocator {

    public GitHubSchemaLocator(final URI uri) {
        super(uri);
    }
    
    protected GitHubSchemaLocator(final URI uri, 
                                  final Map<URI, Map<String, JsonObject>> schemas) {
        super(uri, schemas);
    }

    @Override
    public JsonSchemaLocator resolve(final URI uri) {
        final String path = uri.getPath();
        final StringBuilder file = new StringBuilder(path);
        file.setCharAt(0, Character.toLowerCase(path.charAt(0)));
        file.append(".json");
        return new GitHubSchemaLocator(super.uri.resolve(file.toString()), schemas);
    }
}
