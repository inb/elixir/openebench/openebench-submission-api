/**
 * *****************************************************************************
 * Copyright (C) 2017 ELIXIR ES, Spanish National Bioinformatics Institute (INB)
 * and Barcelona Supercomputing Center (BSC)
 *
 * Modifications to the initial code base are copyright of their respective
 * authors, or their employers as appropriate.
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *****************************************************************************
 */

package es.bsc.inb.elixir.openebench.rest;

import java.net.URI;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

/**
 * @author Dmitry Repchevsky
 */

public class OpenEBenchKeysValidationBean {
    
    private String bench_url;
    
    @Inject private ServletContext context;
    @Inject private HttpServletRequest request;
    
    @PostConstruct
    protected void init() {
        bench_url = context.getInitParameter("benchmarking.url");
    }

    public boolean validatePKs(final Map<String, String> values, 
                               final JsonObject object,
                               final String community) {

        final JsonString _schema = object.getJsonString("_schema");
        final String[] path = _schema.getString().split("/");
        final String collection = path[path.length - 1];
        
        for(String primary_key : values.values()) {
            if (isValid(new StringBuilder(primary_key), collection, community)) {
                return false;
            }
        }
        return true;
    }
    
    public boolean validateFKs(final StringBuilder value, 
                               final JsonArray fkeys,
                               final String community) {
        for (int i = 0, n = fkeys.size(); i < n; i++) {
            final JsonValue fk_schema = fkeys.get(i);
            if (ValueType.OBJECT == fk_schema.getValueType()) {
                if (!isValid(value, fk_schema.asJsonObject(), community)) {
                    return false;
                }
            } else {
                Logger.getLogger(OpenEBenchKeysValidationBean.class.getName()).log(Level.SEVERE, "foreign_key is not an object");
            }
        }
        return true;
    }

    private boolean isValid(final StringBuilder value, 
                            final JsonObject fk_schema,
                            final String community) {

        final JsonValue _id = fk_schema.get("schema_id");
        if (_id == null) {
            Logger.getLogger(OpenEBenchKeysValidationBean.class.getName()).log(Level.SEVERE, "schema_id == null");
            return false;
        }
        if (ValueType.STRING != _id.getValueType()) {
            Logger.getLogger(OpenEBenchKeysValidationBean.class.getName()).log(Level.SEVERE, "schema_id must be STRING");
            return false;
        }
        
        return isValid(value, ((JsonString)_id).getString(), community);
    }
    
    private boolean isValid(final StringBuilder value, 
                            final String collection, 
                            final String community) {
        
        final String id = value.toString();

        final String _id = getId(community, collection, id);
        if (_id == null) {
            return false;
        }

        if (!id.equals(_id)) {
            value.setLength(0);
            value.append(_id);
        }
        
        URI uri = URI.create(request.getRequestURL().toString());
        UriBuilder builder = UriBuilder.fromUri(uri.resolve(context.getContextPath())).path(OpenEBenchService.class).path(collection).path(_id);
        Response response = ClientBuilder.newClient().target(builder).request(MediaType.APPLICATION_JSON).head();
        if (response.getStatus() != 404) {
            return true;
        }
        
        builder = UriBuilder.fromPath(bench_url).path(collection).path(_id);
        response = ClientBuilder.newClient().target(builder).request(MediaType.APPLICATION_JSON).head();
        
        return response.getStatus() != 404;
    }
    
    public static String getId(final String communicty_code,
                               final String collection, 
                               final String id) {

        final char ch;
        switch(collection) {
            case "Dataset":           ch = 'D'; break;
            case "BenchmarkingEvent": ch = 'E'; break;
            case "Challenge":         ch = 'X'; break;
            case "Metrics":           ch = 'M'; break;
            case "MetricsCategory":   ch = 'Y'; break;
            case "TestAction":        ch = 'A'; break;
            case "Tool":              ch = 'T'; break;
            case "Community":   return id.startsWith("OEB") && id.length() == 7 ? id 
                                            : "OEBC" + communicty_code;
            default: return id;
        }

        if (id.startsWith("OEB")) {
            return id.length() == 14 ? id : null;
        }
        
        int hash = 7;
        for (int i = 0; i < id.length(); i++) {
            hash = hash * 31 + id.charAt(i);
        }

        final String code = Integer.toString(Math.abs(hash), 36).toUpperCase();
        
        return "OEB" + ch + communicty_code + "t" + "000000".substring(code.length()) + code;
    }
}
