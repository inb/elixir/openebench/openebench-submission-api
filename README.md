# WITHDRAWN. Has been merged into the openebench-rest-api

###  OpenEBench Java Submission API is an API to submit benchmarking data

###### Enterprise Java 8 (JEE8) Platform
OpenEBench Submission API is strictly adherent to JEE8 specification.
The API is developed and deployed on [WildFly 17.1](http://wildfly.org/) server, 
but should run on other servers (i.e. [Apache Tomcat](http://tomcat.apache.org/)).

###### MongoDB
The data is stored in [MongoDB](www.mongodb.com)

###### Apache Maven build system
To simplify build process API uses [Apache Maven](https://maven.apache.org/) build system.

To build OpenEBench Submission REST application:

Compiling:
```shell
>git clone https://gitlab.bsc.es/inb/elixir/openebench/openebench-submission-api.git
>cd openebench-submission-api
>mvn install
```

##### REST API

- __GET__ /{collection} ```ACCEPT: application/json``` return an array of all collection data objects
- __GET__ /{collection} ```ACCEPT: text/uri-list``` returns a plain list of data identifiers found in the collection
- __GET__ /{collection} ```ACCEPT: text/x-uri``` returns the URI of JSON Schema location for the this collection data objects
- __GET__ /{collection} ```ACCEPT: application/schema+json``` return the JSON Schema that corresponds the data objects for the collection
- <sub><sup>&#x1F512;</sup></sub> __POST__ /{collection} ```ACCEPT: application/json``` put multiple data objects into the collection. the id is taken from an object '_id' attribute 
---
- __GET__ /{collection}/{id} return data object reffered by its id
- __HEAD__ /{collection}/{id} check whether the object exists
- <sub><sup>&#x1F512;</sup></sub> __PUT__ /{collection}/{id} ```Content-Type: application/json``` put data object into collection
- <sub><sup>&#x1F512;</sup></sub> __PATCH__ /{collection}/{id} ```Content-Type: application/json``` patches data object using mongodb 'upsert' operation
- <sub><sup>&#x1F512;</sup></sub> __DELETE__ /{collection}/{id} remove data object from the collection
---
- <sub><sup>&#x1F512;</sup></sub> __POST__ / ```Content-Type: application/json``` put a list of data objects into the storage. the object collection and id is taken from the object '_schema' and  '_id' attributes
